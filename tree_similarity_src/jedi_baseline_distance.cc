/*
adapted from "src/command_line/main.cc" in
"https://github.com/DatabaseGroup/tree-similarity"  provided using the
following license:

MIT License

Copyright (c) 2021 Database Research Group Salzburg

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <unordered_map>
#include "unit_cost_model.h"
#include "json_label.h"
#include "node.h"
#include "bracket_notation_parser.h"
#include "label_intersection.h"
#include "tree_indexer.h"
#include "jedi_baseline_index.h"

// argc argument name omitted because not used.
int main(int, char** argv) {

  // Input file name.
  std::string input_file_name = std::string(argv[1]);

  // Type aliases.
  using Label = label::JSONLabel;
  using CostModel = cost_model::UnitCostModelJSON<Label>;
  using LabelDictionary = label::LabelDictionary<Label>;
  using TreeIndexer = node::TreeIndexJSON;
  using JEDIBASE = json::JEDIBaselineTreeIndex<CostModel, TreeIndexer>;
  
  // Initialize label dictionary - separate dictionary for each test tree
  // because it is easier to keep track of label ids.
  LabelDictionary ld;
  
  // Initialize cost model.
  CostModel ucm(ld);
  JEDIBASE baseline_algorithm(ucm);
  
  // Initialize two tree indexes.
  // Use TreeIndexAll that is a superset of all algorithms' indexes.
  TreeIndexer ti1;
  TreeIndexer ti2;

  // Create the container to store all trees.
  std::vector<node::Node<Label>> trees_collection;
  
  // Parse the dataset.
  parser::BracketNotationParser<Label> bnp;
  bnp.parse_collection(trees_collection, input_file_name);

  // iterate over all trees in the given collection
  for (unsigned int i = 0; i < trees_collection.size()-1; i++) {
    // Index input trees.
    node::index_tree(ti1, trees_collection[i], ld, ucm);
    for (unsigned int j = i+1; j < trees_collection.size(); j++) {
    node::index_tree(ti2, trees_collection[j], ld, ucm);

    // Compute the label intersection for two consecutive trees.
    double jedi = baseline_algorithm.jedi(ti1, ti2);

      std::cout << "JEDI distance between trees <" << i+1 << "> and <" << j+1 << ">: " << jedi << std::endl;
    }
  }
  
  return 0;
}
