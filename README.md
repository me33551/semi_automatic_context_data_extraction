# semi_automatic_context_data_extraction

This repository contains the code used for the evaluation of the paper "Enhancing Process Models by Semi-Automatic Context Data Annotation Based on Process Logs"

The two folders represent the evaluation on an artificial as well as on part of a real-world data set.

In order to execute the following steps need to be taken:
- install python and ruby libraries
- clone the repository available at https://github.com/DatabaseGroup/tree-similarity.git and then perform the following steps:
  - navigate into repository
  - add the following to the end of "src/command_line/CMakeLists.txt":
  ```
  # Build executable for out tool.
  add_executable(
    jedi_distance # EXECUTABLE NAME
    jedi_baseline_distance.cc # EXECUTABLE SOURCE
  )
  
  # Let the compiler know to find the header files in TreeSimilarity library.
  target_link_libraries(
    jedi_distance # EXECUTABLE NAME
    TreeSimilarity # LIBRARY NAME
  )
  
  # Build executable for out tool.
  add_executable(
    quickjedi_distance # EXECUTABLE NAME
    jedi_quickjedi_distance.cc # EXECUTABLE SOURCE
  )
  
  # Let the compiler know to find the header files in TreeSimilarity library.
  target_link_libraries(
    quickjedi_distance # EXECUTABLE NAME
    TreeSimilarity # LIBRARY NAME
  )
  ```
  - copy the files contained in "tree_similarity_src" to "src/command_line"
  - then build with:
  ```
  mkdir build
  cd build
  cmake ..
  make
  ```
  - create directory "jedi_distance" in the folder where you want to perform the evaluation (i.e., "evaluation_artificial" or "evaluation_real_world")
  - inside the "jedi_distance" directory create symbolic links to the files "build/jedi_distance" and "build/quickjedi_distance"
- make the python and ruby files executable (chmod +x [file_name]) 
- execute either "sh execute_a_eval.sh 0.1" or "sh execute_b_eval.sh 0.1" (for the meaning of the parameters and different folders please refer to the paper)
- if you want to execute it again, first call "sh clean_up.sh"
