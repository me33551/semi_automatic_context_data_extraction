#!/usr/bin/python3

import math
import csv
import sys
import json
import numpy as np
from sklearn.cluster import DBSCAN
from sklearn import metrics
from pathlib import Path 
from sklearn.neighbors import NearestNeighbors
from kneed import KneeLocator
from matplotlib import pyplot as plt


# argv[1] - 'structural' or 'value_based'
# argv[2] - if 'structural' determines epsilon for DBSCAN, if 'value_based' determines neighbors for NearestNeighbors 

nodes = []
distances = []
X = []

if(len(sys.argv) < 2):
  print("choose either 'structural' or 'value_based' as first argument")
elif(sys.argv[1] == "structural"):
  with open(Path(__file__).parent / 'distances/first_step/distances.csv', newline='') as csvfile:
    file = csv.reader(csvfile, delimiter=',')
    not_first = False
    for row in file:
      index = row.pop(0)
      if(not_first):
        nodes.append(index)
        distances.append(list(map(lambda el: int(el), row)))
        #distances.append(row)
      not_first = True
  
  #print(distances[0])
  
  X = distances
  
  #db = DBSCAN(eps=2, min_samples=1,metric="precomputed").fit(X)
  db = DBSCAN(eps=float(sys.argv[2]),min_samples=1,metric="precomputed").fit(X)
  labels = db.labels_
  print(labels)
  
  # Number of clusters in labels, ignoring noise if present.
  clusters = len(set(labels)) - (1 if -1 in labels else 0)
  big_clusters = len(set([el for el in labels if list(labels).count(el) > 1])) - (1 if -1 in labels else 0)
  noise = list(labels).count(-1)
  
  #print(labels)
  
  print(f"Estimated number of clusters: {clusters}")
  print(f"Estimated number of clusters with >1 elements: {big_clusters}")
  print(f"Estimated number of noise points: {noise}")
  
  with open(Path(__file__).parent / 'clusters/first_clustering/clusters.csv', 'w') as csvfile:
      writer = csv.writer(csvfile, delimiter=',')
      writer.writerow(["node","label"])
      counter = 0
      for label in labels:
        #writer.writerow([counter,label])
        writer.writerow([nodes[counter],label])
        counter += 1
elif(sys.argv[1] == "value_based"):

  #nodes = []
  #distances = []
  #X = []

  weights = {"string":1,"date":1,"numeric":1}
  #weights = {"string":1,"date":0.1,"numeric":1}
  #weights = {"string":0.5,"date":0.1,"numeric":1}

  with open(Path(__file__).parent / f'distances/first_step/distances.csv', newline='') as csvfile:
    file = csv.reader(csvfile, delimiter=',')
    not_first = False
    #long_enough = True if(file.line_num > int(sys.argv[2])+1) else False
    long_enough = False
    for row in file:
      if(file.line_num > int(sys.argv[2])+1):
        long_enough = True
      index = row.pop(0)
      #print(index)
      if(not_first):
        nodes.append(index)
        x = list(map(lambda el: json.loads(el), row))
        distances.append(x)
      not_first = True
    d = np.array(distances).flatten()
    max_dist = {}
    unique_keys = np.unique(np.concatenate(list(map(lambda el: list(el),d)),dtype=object).tolist())
    print(unique_keys)
    weights_sum = 0
    for k in list(weights.keys()):
      weights_sum += weights[k]
    for k in unique_keys:
      max_el = max(d, key = lambda x : x[k] if(k in x) else 0)
      max_dist[k] = max_el[k]
      #weights_sum += weights[k]

    #print(distances)
    #print(weights_sum)
    #print(max_dist)

    for dist_array in distances:
      row = []
      for dist in dist_array:
        overall_dist = 0.0
        types = list(weights.keys())
        c = 0
        for k,v in dist.items():
          #print(k)
          types.remove(k)
          if(float(max_dist[k] == 0.0)):
            # dist[k] ist hier dann auch 0
            # do nothing (multiplication with 0)  as overall_dist should not be increased
            overall_dist += (float(weights[k])/float(weights_sum)) * float(dist[k])
          else:
            overall_dist += (float(weights[k])/float(weights_sum)) * (float(dist[k]) / float(max_dist[k]))
          if(weights[k] != 0):
            c += 1
        for t in types:
          overall_dist += (float(weights[k])/float(weights_sum))
        #if(c == 0):
        #  overall_dist = weights_sum
        row.append(overall_dist)
      X.append(row)
    #print(len(X))
    #print(len(X[1]))
    #print(len(nodes))
    #print(len(X))
    #print(len(distances))
    #print(X)
    """
    for t in X:
      #print(len(t))
      #print(list(filter(lambda x : x < 0.00001,t)))
      print(list(map(lambda el:round(el,3),t)))
      print(t.index(0.0))
    """
    print(long_enough)

  if(long_enough):
    neigh = NearestNeighbors(n_neighbors=int(sys.argv[2]),metric="precomputed")
    nbrs = neigh.fit(X)
    dist, indices = nbrs.kneighbors(X)
    
    dist = np.sort(dist, axis=0)
    dist = dist[:,1]
    #print(dist)
    
    kl = KneeLocator(
        list(range(0,len(dist))),
        dist,
        curve='convex',
        direction='increasing',
        interp_method='polynomial',
    )
    print(kl.elbow)
    print(f"identified {dist[kl.elbow]} as elbow")
    if(kl.elbow is None):
      print("no elbow found - next iteration")
      exit()
    if(type(kl.elbow) is list):
      print("multiple elbows found - next iteration")
      exit()

    print("show plot")
    plt.plot(dist)
    plt.show()
    
    
    db = DBSCAN(eps=dist[kl.elbow],min_samples=1,metric="precomputed").fit(X)
    #db = DBSCAN(eps=0.000000001,min_samples=1,metric="precomputed").fit(X)
    labels = db.labels_
    
    # Number of clusters in labels, ignoring noise if present.
    clusters = len(set(labels)) - (1 if -1 in labels else 0)
    big_clusters = len(set([el for el in labels if list(labels).count(el) > 1])) - (1 if -1 in labels else 0)
    noise = list(labels).count(-1)
    
    print(labels)
    print(len(labels))
    print(f"Estimated number of clusters: {clusters}")
    print(f"Estimated number of clusters with >1 elements: {big_clusters}")
    print(f"Estimated number of noise points: {noise}")
    with open(Path(__file__).parent / f'clusters/first_clustering/clusters.csv', 'w') as csvfile:
      writer = csv.writer(csvfile, delimiter=',')
      writer.writerow(["node","label"])
      counter = 0
      for label in labels:
        writer.writerow([nodes[counter],label])
        counter += 1
 
else:
  print("choose either 'structural' or 'value_based' as first argument")
