mkdir wastebasket
mv artificial_structs wastebasket
mkdir artificial_structs
mv jedi_distance/data.txt wastebasket
mv jedi_distance/data_full.txt wastebasket
mv distances wastebasket
mkdir distances
mkdir distances/first_step
mv clusters/first_clustering wastebasket
mkdir clusters/first_clustering
mkdir distances/second_step
mv clusters/second_clustering wastebasket
mkdir clusters/second_clustering

./first_step.rb structural '*' 4
./first_clustering.py structural $1
./second_step.rb value_based '*' '*'
./second_clustering.py value_based 2
