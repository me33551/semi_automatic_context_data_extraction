#!/usr/bin/env ruby
require "psych"
require "json"

def get_keys(data) #{{{
  if(data.is_a?(Hash)) then
    ret = {}
    data.keys.each() do |datum|
      ret[datum] = get_keys(data[datum])
    end
  elsif(data.is_a?(Array)) then
    ret = []
    (0...data.size()).to_a().each() do |datum|
      ret[datum] = get_keys(data[datum])
    end
  else
    ret = data
  end
  return ret
end #}}}

def get_stats(data) #{{{
  if(data.is_a?(Hash)) then
    ret = {}
    data.each() do |key,datum|
      #p "key #{key}"
      #p "datum #{datum}"
      if(ret.dig(key,:alternatives).nil?) then
        ret[key] = {:alternatives => []}
      end
      datum_c = classify(datum)
      matches = ret[key][:alternatives].select() {|el| datum_c == el[:schema]}
      if(matches.empty?()) then
        ret[key][:alternatives] << {:schema => classify(datum), :occurrences => 1}
      else
        matches.each() do |match|
          match[:occurrences] += 1
        end
      end
      sum = ret[key][:alternatives].sum(0.0) {|el| el[:occurrences]}
      ret[key][:alternatives].each() do |alt|
        alt[:probability] = alt[:occurrences]/sum
      end
      ret[key][:sub] = get_stats(datum)
    end
  elsif(data.is_a?(Array)) then
    ret = {:alternatives => [], :subs => []}
    data.each() do |datum|
      #p "key #{key}"
      #p "datum #{datum}"
      datum_c = classify(datum)
      #p ret
      m = ret[:alternatives].select() {|el| datum_c == el[:schema]}
      if(m.empty?()) then
        #element = ret.last()
        ret[:alternatives] << {:schema => datum_c, :occurrences => 1}
        #p "found none"
      elsif(m.size() == 1)
        m.each() do |match|
          match[:occurrences] += 1
        end
        #element = m.first()
        #p "found one"
      else
        throw "asdfg"
      end
      #p datum_c
      sum = ret[:alternatives].sum(0.0) {|el| el[:occurrences]}
      ret[:alternatives].each() do |alt|
        alt[:probability] = alt[:occurrences]/sum
      end
      #matches = element[:alternatives].select() {|el| datum_c == el[:schema]}
      #matches = element[:alternatives].select() {|el| return true}
      #p matches
      #if(matches.empty?()) then
      #  element[:alternatives] << {:schema => datum_c, :occurrences => 1}
      #else
      #  matches.each() do |match|
      #    match[:occurrences] += 1
      #  end
      #end
      #sum = element[:alternatives].sum(0) {|el| el[:occurrences]}
      #element[:alternatives].each() do |alt|
      #  alt[:probability] = alt[:occurrences]/sum
      #end
      ret[:subs] << get_stats(datum)
    end

  else
    ret = classify(data)
  end
  return ret 
end #}}}

def get_stats_values(data) #{{{
  if(data.is_a?(Hash)) then
    ret = {}
    data.each() do |key,datum|
      #p "key #{key}"
      #p "datum #{datum}"
      if(ret.dig(key,:alternatives).nil?) then
        ret[key] = {:alternatives => []}
      end
      datum_c = datum
      matches = ret[key][:alternatives].select() {|el| datum_c == el[:schema]}
      if(matches.empty?()) then
        ret[key][:alternatives] << {:schema => datum, :occurrences => 1}
      else
        matches.each() do |match|
          match[:occurrences] += 1
        end
      end
      sum = ret[key][:alternatives].sum(0.0) {|el| el[:occurrences]}
      ret[key][:alternatives].each() do |alt|
        alt[:probability] = alt[:occurrences]/sum
      end
      ret[key][:sub] = get_stats_values(datum)
    end
  elsif(data.is_a?(Array)) then
    ret = {:alternatives => [], :subs => []}
    data.each() do |datum|
      #p "key #{key}"
      #p "datum #{datum}"
      datum_c = datum
      #p ret
      m = ret[:alternatives].select() {|el| datum_c == el[:schema]}
      if(m.empty?()) then
        #element = ret.last()
        ret[:alternatives] << {:schema => datum_c, :occurrences => 1}
        #p "found none"
      elsif(m.size() == 1)
        m.each() do |match|
          match[:occurrences] += 1
        end
        #element = m.first()
        #p "found one"
      else
        throw "asdfg"
      end
      #p datum_c
      sum = ret[:alternatives].sum(0.0) {|el| el[:occurrences]}
      ret[:alternatives].each() do |alt|
        alt[:probability] = alt[:occurrences]/sum
      end
      #matches = element[:alternatives].select() {|el| datum_c == el[:schema]}
      #matches = element[:alternatives].select() {|el| return true}
      #p matches
      #if(matches.empty?()) then
      #  element[:alternatives] << {:schema => datum_c, :occurrences => 1}
      #else
      #  matches.each() do |match|
      #    match[:occurrences] += 1
      #  end
      #end
      #sum = element[:alternatives].sum(0) {|el| el[:occurrences]}
      #element[:alternatives].each() do |alt|
      #  alt[:probability] = alt[:occurrences]/sum
      #end
      ret[:subs] << get_stats_values(datum)
    end

  else
    ret = (data)
  end
  return ret 
end #}}}

def has_same_keys(data1, data2) #{{{
  return classify(data1) == classify(data2)
end

def classify(data)
  if(data.is_a?(Hash)) then
    return (data.map() {|k,d| [k, classify(d)]}).to_h
  elsif(data.is_a?(Array)) then
    return data.map() {|d| classify(d)}
  else
    return data.class()
  end
end #}}}


#filecontent = IO.read("./gv12/logs/batch14/ce641421-f663-4eeb-8894-b6b243903ff8.xes.yaml", {:encoding => "utf-8"})
#ce641421-f663-4eeb-8894-b6b243903ff8
filecontent = IO.read("./artificial_data.json", mode: "r:UTF-8")
#filecontent = IO.read("./example.yaml", mode: "r:UTF-8")

event_list = JSON.load(filecontent) 

#model = event_list.first.dig("log","trace","cpee:name")
model = "artificial" 

structures = []
structures_values = []

event_list.each do |object|
  #event_id = object.dig("event","id:id")
  #name = object.dig("event","concept:name")
  #data = object.dig("event","data","data_receiver")
  
  data = object

  if(!data.nil?()) then
    x = get_keys(data)
    struct = get_stats(x)
    struct_val = get_stats_values(x)
    structures << struct
    structures_values << struct_val
  end


  #p "#{model} / #{name} / #{event_id}"
end
p structures.size() 
p structures_values.size() 

structures.each_with_index do |struct,index|
  File.open(File.join(__dir__,'artificial_structs',"#{format('%03d',index)}_artificial_data_struct.json"), 'w') do |file|
    file.write("#{struct.to_json}\n")
  end
end

structures_values.each_with_index do |struct,index|
  File.open(File.join(__dir__,'artificial_structs',"#{format('%03d',index)}_artificial_data_struct_values.json"), 'w') do |file|
    file.write("#{struct.to_json}\n")
  end
end
