#!/usr/bin/env ruby
require 'json'
require 'csv'
require 'rubytree'
require 'systemu'
require 'levenshtein'


# inputs:
#   - structure 1
#   - structure 2
#   - similarity function -> zusammengesetzt aus verschiedenen funktionen ????

# output:
#   - similarity score between 0 and 1

def classify(data) #{{{
  if(data.is_a?(Hash)) then
    return (data.map() {|k,d| [k, classify(d)]}).to_h
  elsif(data.is_a?(Array)) then
    return data.map() {|d| classify(d)}
  else
    return data.class()
  end
end #}}}

def search(pattern,structure) #{{{
  
  ret = []
  if(structure.is_a?(Hash) && structure.keys.include?("alternatives")) then
    r = structure['alternatives'].filter() { |element|  x = classify(element['schema']); pattern == '*' || x == pattern || x.to_s == pattern }
    rr = []
    r.each do |el|
      el['occurrences'].times() do |_|
        rr.append(el)
      end
    end
    ret.append(*rr)
    structure['subs'].each() do |sub|
      result = search(pattern, sub)
      ret.append(*result)
    end
  elsif(structure.is_a?(Hash) && !structure.keys.include?("alternatives"))
    structure.each() do |key,value|
      r = value['alternatives'].filter() { |element| x = classify(element['schema']); pattern == '*' || x == pattern || x.to_s == pattern }
      rr = []
      r.each do |el|
        el['occurrences'].times() do |_|
          rr.append(el)
        end
      end
      ret.append(*rr)
      result = search(pattern, value['sub'])
      ret.append(*result)
    end
  elsif(!structure.is_a?(Hash))
    #p "reached end of the branch"
  else
    p "?!"
  end

  return ret
end #}}}

def transform(json) #{{{
  obj = JSON.parse(json)
  #p "currently at #{obj.to_json()}"
  if(obj.is_a?(Hash)) then
    ret = Tree::TreeNode.new("{}", "object")
    #p "insert in hash #{obj}"
    obj.each() do |key,value|
      #p "insert in hash #{key} - #{value}"
      ret << Tree::TreeNode.new(key, "key") << transform(value.to_json)
    end
    #p "insert in hash #{obj} - after each"
    return ret
  elsif(obj.is_a?(Array)) then
    ret = Tree::TreeNode.new("[]", "array")
    obj.each_with_index() do |value,index|
      ret << transform(value.to_json)
      #p index
      ret.last_child().rename("{}_#{index}")
      #ret << Tree::TreeNode.new("soething_#{index}", "object") << transform(value.to_json)
    end
    return ret
  else
    return Tree::TreeNode.new(obj.nil?() ? "nil" : obj, "literal")
  end
  return ret
end #}}}

def to_bracket_notation(tree) #{{{
  string = ""
  string += '{'
  #string += tree.name.gsub('{','\{') .gsub('}','\}')
  if(tree.name.is_a?(String) && tree.name != '{}' && !(tree.name =~ /{}_.+/) && tree.name != '[]') then 
    string += '"'
  end
  if(tree.name =~ /{}_.+/) then
    string += '{}'.gsub('{','\{') .gsub('}','\}')
  else
    string += tree.name.to_s.gsub('{','\{') .gsub('}','\}')
  end
  if(tree.name.is_a?(String) && tree.name != '{}' && !(tree.name =~ /{}_.+/) && tree.name != '[]') then 
    string += '"'
  end
  string += ':' if tree.content == "key"
  tree.children() do |child|
    string += to_bracket_notation(child)
  end
  string += '}'
  return string
end #}}}

def jedi_quickjedi(*jsons) #{{{
  jsons_uniq = []
  trees = []
  jsons_new = []
  #jsons.each_with_index() do |json,index|
  jsons.each() do |j|
    json = j[:value]
    #p "transforming #{json}"
    tree = transform(json)
    #if(tree.node_height() <= ARGV[1].to_i() || ARGV[1] == '*') then
    if((tree.node_height() >= ARGV[1].to_i() || ARGV[1] == '*') && (tree.node_height() <= ARGV[2].to_i() || ARGV[2] == '*')) then
    #if(true) then
      jsons_new.push({:node => j[:node], :json => json})
      if(!jsons_uniq.include?(json)) then
        trees.push({:node => j[:node], :tree => tree})
        jsons_uniq.push({:node => j[:node], :json => json})
      end
    end
  end
  jsons = jsons_new
  #trees.each() do |tree|
    #tree.print_tree()
    #p tree.node_height()
  #end
  #trees.filter!() { |t| t.node_height <= 2 }
  #p "transformation finished"
  File.open(File.join(__dir__,'jedi_distance','data.txt'), mode="w") do |f|
    f << trees.map() do |tree|
      to_bracket_notation(tree[:tree])
    end.join("\n")
  end
  status, stdout, stderr = systemu("./jedi_distance/quickjedi_distance ./jedi_distance/data.txt")
  if(status.success?) then
    results = stdout.split("\n")
    #p results
    ret = df_matrix = Array.new(jsons.size()) {Array.new(jsons.size(),nil)}
    #(0...jsons_new.size()).each() do |i|
    #  ret[i][i] = 0
    #end
    m_uniq = df_matrix = Array.new(jsons_uniq.size()) {Array.new(jsons_uniq.size(),nil)}
    (0...jsons_uniq.size()).each() do |i|
      m_uniq[i][i] = 0
    end
    results.each() do |result|
      m = result.match(/JEDI distance between trees *<(.*)> and <(.*)>: (.*)/)
      m_uniq[m[1].to_i-1][m[2].to_i-1] = m[3].to_i
      m_uniq[m[2].to_i-1][m[1].to_i-1] = m[3].to_i

      #m = result.match(/JEDI distance between trees *<(.*)> and <(.*)>: (.*)/)
      #p m
      #ret[m[1].to_i-1][m[2].to_i-1] = m[3].to_i
      #ret[m[2].to_i-1][m[1].to_i-1] = m[3].to_i
    end
    #p jsons.size()
    #p jsons_new.size()
    #p jsons_uniq.size()
    jsons.each_with_index() do |json_outer,index_outer|
      jsons.each_with_index() do |json_inner,index_inner|
        #ret[index_outer][index_inner] = m[3].to_i
        #p json_outer
        #p json_inner
        #p m_uniq
        a = jsons_uniq.index(json_outer)
        b = jsons_uniq.index(json_inner)
        #p "#{a}, #{b}"
        ret[index_outer][index_inner] = m_uniq[a][b]
        ret[index_inner][index_outer] = m_uniq[b][a]
      end
    end
    retu = []
    ret.each_with_index() do |r,index|
      retu.push({:node => jsons[index][:node], :values => r})
    end
  else
    p "error calling json distance function"
    retu = []
  end
  p "returning"
  return retu
end #}}}

def string_distance(a,b) #{{{
  return Levenshtein.distance(a,b)
end #}}}

def numeric_distance(a,b) #{{{
  return (a-b).abs()
end #}}}

def date_distance(a,b) #{{{
  return (a.to_time() - b.to_time()).abs()
end #}}}

def get_distance(value_array_a, value_array_b) #{{{
  distances_a = Array.new(value_array_a.size()) {Array.new(value_array_b.size(),nil)}
  distances_b = Array.new(value_array_b.size()) {Array.new(value_array_a.size(),nil)}
  value_array_a.each_with_index() do |a,index_a|
    value_array_b.each_with_index() do |b,index_b|
      if(a.is_a?(Numeric) && b.is_a?(Numeric))
        # compare numbers and add to overall_distances
        distances_a[index_a][index_b] = {:type => "numeric", :value => numeric_distance(a,b)}
      elsif(a.is_a?(String) && b.is_a?(String))
        if((a =~ /\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d.\d\d\d[+-]\d\d:\d\d/) == 0  && (b =~ /\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d.\d\d\d[+-]\d\d:\d\d/) == 0) then 
          # compare dates 
          x = DateTime.strptime(a, "%Y-%m-%dT%H:%M:%S.%L%:z")
          y = DateTime.strptime(b, "%Y-%m-%dT%H:%M:%S.%L%:z")
          # add to overall distance
          distances_a[index_a][index_b] = {:type => "date", :value => date_distance(x,y)}
        elsif((a =~ /\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d.\d\d\d[+-]\d\d:\d\d/) != 0  && (b =~ /\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d.\d\d\d[+-]\d\d:\d\d/) != 0)
          # compare strings and add to overall distance
          distances_a[index_a][index_b] = {:type => "string", :value => string_distance(a,b)}
        else
          # cannot compare - set to nil
          distances_a[index_a][index_b] = nil
        end
      else
        # cannot compare - set to nil
        distances_a[index_a][index_b] = nil
      end 
      distances_b[index_b][index_a] = distances_a[index_a][index_b]
    end
  end
  ret = []
  distances_a.each() do |d|
    #p "a: #{d} => #{d.compact()} => #{d.compact().min() { |a,b| a[:value] <=> b[:value] }}"
    ret.push(d.compact().min() { |a,b| a[:value] <=> b[:value] } )
  end
  distances_b.each() do |d|
    #p "b: #{d} => #{d.compact()} => #{d.compact().min() { |a,b| a[:value] <=> b[:value] }}"
    ret.push(d.compact().min() { |a,b| a[:value] <=> b[:value] } )
  end
  return ret
end #}}}

def get_values(object) #{{{
  vals = []
  if(object.is_a?(Hash)) then
    object.map() { |k,v| get_values(v)}.each() { |el| vals.push(*el)}
    return vals
    #return vals.push(*object.map() { |k,v| get_values(v)})
  elsif(object.is_a?(Array))
    object.map() { |v| get_values(v)}.each() { |el| vals.push(*el)}
    return vals
  else
    return vals.push(object)
  end
end

def find_new(group,neighbours)
  ret = []
  group.each() do |el|
    ret.push(*neighbours[el])
  end
  #p ret - group
  return ret.uniq() - group
end #}}}



# ARGV[0]: 'structural' or 'value_based'
# ARGV[1]: if 'structural' determines min height of tree, if 'value_based' determines min number of values; '*' allows everything
# ARGV[2]: if 'structural' determines max height of tree, if 'value_based' determines max number of values; '*' allows everything

content = CSV.read(File.join(__dir__,'clusters','first_clustering','clusters.csv'))

cluster_number =  content[1..-1].max() { |x,y| x[1].to_i() <=> y[1].to_i() }[1].to_i() + 1
p "number of clusters: #{cluster_number}"

groups = Array.new(cluster_number) {{:group=> nil, :values => []}}
#p groups
content[1..-1].each() do |row|
  groups[row[1].to_i()][:group] = row[1].to_i()
  groups[row[1].to_i()][:values].push(row[0].to_i())
end
p groups


full_data = File.read(File.join(__dir__,'jedi_distance','data_full.txt'))
full_data_lines = full_data.split("\n")
full_data_jsons = full_data_lines.map() do |full_data_line|
  JSON.parse(full_data_line)
end
#p full_data


if(ARGV[0] == "structural") then
  groups.each() do |g|
    group = g[:values]
    #if(group.size() > 1) then
    if(true) then
      p "group #{g[:group]} has #{group.size()} elements: #{group}"
      buffer = []
      group.each() do |g|
        buffer.push(:node => g, :value => full_data_jsons[g-1])
      end
      classified_jsons = buffer.map() {|el| {:node => el[:node], :value => classify(el[:value]).to_json()} }
      #p classified_jsons
      x_classified = jedi_quickjedi(*classified_jsons)
      p x_classified
      #p found.size()
      #p x_classified.size()
    end
    CSV.open(File.join(__dir__,'distances','second_step',"cluster_#{g[:group]}_distances.csv"), "w") do |csv|
      #csv << (1..x_classified.size()).to_a.unshift("node")
      csv << x_classified.map() { |el| el[:node] }.unshift('node')
      x_classified.each_with_index() do |row|
      #x_classified.each_with_index() do |row,index|
        #csv << row.unshift(index+1)
        csv << row[:values].unshift(row[:node])
      end
    end
  end

elsif(ARGV[0] == "value_based")

  groups.each() do |g|
    group = g[:values]
    #if(group.size() > 1) then
    if(true) then
      p "group #{g[:group]} has #{group.size()} elements: #{group}"
      buffer = []
      group.each() do |g|
        buffer.push(:node => g, :value => full_data_jsons[g-1])
      end
      #p buffer
      all_values = []
      buffer.each() do |object|
        values = get_values(object[:value])
        all_values.push(:node => object[:node], :values => values)
      end
      #p all_values

      s = 0.0
      counter = 0
      #distances = Array.new(all_values.size()) {{:node => nil, :values => Array.new(all_values.size(),nil)}}
      distances = []
      all_values.each() do |obj_a|
        distances.push({:node => obj_a[:node], :values => []})
        all_values.each() do |obj_b|
          #if((obj_a[:values].size() <= ARGV[1].to_i() && obj_b[:values].size() <= ARGV[1].to_i()) || ARGV[1] == '*') then
          if(((obj_a[:values].size() >= ARGV[1].to_i() && obj_b[:values].size() >= ARGV[1].to_i()) || ARGV[1] == '*') && ((obj_a[:values].size() <= ARGV[2].to_i() && obj_b[:values].size() <= ARGV[2].to_i()) || ARGV[2] == '*')) then
            counter += 1
            #if(distances[counter][:values].nil?()) then
              dist = get_distance(obj_a[:values],obj_b[:values])
              distances.last[:values].push({})
              dist.compact().map() { |el| el[:type] }.uniq().each() do |type|
                #sum_type = dist.compact().sum(0.0) { |el|  el[:type] == type ? el[:value] : 0.0 }
                min_type = dist.compact().filter() { |el|  el[:type] == type}.min() { |a,b| a[:value] <=> b[:value]}
                distances.last[:values].last[type] = min_type[:value]
                #distances[index_a][index_b][type] = sum_type
                #p "  #{type}: #{sum_type}"
              #end
            end
          end
        end
        if(distances.last[:values]).empty?() then
          distances.pop()
        end
      end
      p distances

      # write to file(s)
      CSV.open(File.join(__dir__,'distances','second_step',"cluster_#{g[:group]}_distances.csv"), "w") do |csv|
        csv << distances.map() { |el| el[:node] }.unshift("node")
        distances.each() do |el|
          csv << [el[:node]] + el[:values].map() { |el| el.to_json() }
        end
      end


    end
  end

else
  p "choose either 'structural' or 'value_based'"
end
